﻿using System;
using System.Diagnostics;

namespace SelectionSort
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("\n\nEnter the total number of elements: ");
            int max = Convert.ToInt32(Console.ReadLine());
            string[] arr = new string[1];

        Found:
            arr = Console.ReadLine().ToString().Split(',');

            if (arr.Length == max)
            {
                int n = max;
                Stopwatch sw2 = new Stopwatch();
                sw2.Start();

                for (int x = 0; x < n; x++)
                {
                    int min_index = x;
                    for (int y = x; y < n; y++)
                    {
                        if (int.Parse(arr[min_index]) > int.Parse(arr[y]))
                        {
                            min_index = y;
                        }
                    }
                    int temp = int.Parse(arr[x]);
                    arr[x] = arr[min_index];
                    arr[min_index] = temp.ToString();
                }
                Console.WriteLine("\n Sort array in ascending order using Selection Sort");



                foreach (string i in arr)
                {
                    Console.Write(i + ",");
                }

                sw2.Stop();
                Console.WriteLine("\n\n Selection Sort: " + sw2.Elapsed);
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("\n record not the same with maximum number of array selected");
                goto Found;
            }
            Console.ReadLine();
        }

   
    }
}
